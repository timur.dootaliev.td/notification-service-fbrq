from django.db import models


class Client(models.Model):
    """Модель клиента"""

    phone = models.CharField(max_length=11)
    operator_code = models.CharField(max_length=3, blank=True)
    tag = models.CharField(max_length=10)
    timezone = models.CharField(max_length=255)

    def __str__(self) -> str:
        return self.phone

    def save(self, *args, **kwargs):
        self.operator_code = self.phone[1:4]
        super().save(*args, **kwargs)

    class Meta:
        default_related_name = "clients"
