from django.core.management.base import BaseCommand
from applications.clients.models import Client
from faker import Faker
import re
import random


class Command(BaseCommand):
    help = "Creates 10 random clients"

    def handle(self, *args, **options):
        fake = Faker("ru_RU")

        tags = ["tag1", "tag2", "tag3", "tag4"]

        timezones = [
            "Europe/Kaliningrad",
            "Europe/Moscow",
            "Europe/Samara",
            "Asia/Yekaterinburg",
            "Asia/Omsk",
            "Asia/Novosibirsk",
            "Asia/Krasnoyarsk",
            "Asia/Irkutsk",
            "Asia/Yakutsk",
            "Asia/Vladivostok",
        ]
        clients = []
        for _ in range(10):
            phone = "".join(re.findall("[0-9]", fake.phone_number()))[1:]
            client = Client(
                phone="7" + phone,
                tag=random.choice(tags),
                timezone=random.choice(timezones),
            )
            clients.append(client)

        Client.objects.bulk_create(clients, update_fields=["operator_code"])

        for client in Client.objects.all():
            client.save()

        self.stdout.write("Clients created successfully")
