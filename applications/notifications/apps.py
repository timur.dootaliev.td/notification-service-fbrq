from django.apps import AppConfig
from django.db.models.signals import post_save


class NotiificationsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "applications.notifications"

    def ready(self) -> None:
        from .signals import create_messages
        from .models import MessageDistribution

        post_save.connect(create_messages, MessageDistribution)
