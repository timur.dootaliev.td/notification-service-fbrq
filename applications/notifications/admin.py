from django.contrib import admin
from .models import Message, MessageDistribution
from .forms import DistributionForm


@admin.register(MessageDistribution)
class MessageDistributionAdmin(admin.ModelAdmin):
    list_filter = ["operator_code", "tag"]
    form = DistributionForm


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_filter = ["is_distributed"]
