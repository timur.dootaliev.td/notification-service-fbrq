from rest_framework import serializers

from .models import Message, MessageDistribution


class ListMessageDistributionSerializer(serializers.ListSerializer):
    def to_representation(self, data):
        representation = super().to_representation(data)
        return [
            {
                "id": obj["id"],
                "operator_code": obj["operator_code"],
                "tag": obj["tag"]
                }
            for obj in representation
        ]


class MessageDistributionSerializer(serializers.ModelSerializer):
    class Meta:
        model = MessageDistribution
        fields = [
            "id",
            "text",
            "operator_code",
            "tag",
            "distribution_start",
            "distribution_end",
        ]
        list_serializer_class = ListMessageDistributionSerializer

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation["distributed"] = MessageSerializer(instance.messages.filter(
            is_distributed=True
        ), many=True).data
        representation['distributed_count'] = len(representation['distributed'])
        representation["not_distributed"] = MessageSerializer(instance.messages.filter(
            is_distributed=False
        ), many=True).data
        representation['not_distributed_count'] = len(representation['not_distributed'])
        return representation


class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Message
        fields = [
            "id",
            "client",
            "distribution",
            "is_distributed",
            "created_at",
        ]
