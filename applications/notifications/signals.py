from .models import MessageDistribution
from .tasks import _create_messages


def create_messages(sender, instance: MessageDistribution, *args, **kwargs):
    return _create_messages.apply_async(
        args=[instance.pk],
        eta=instance.distribution_start
        )
