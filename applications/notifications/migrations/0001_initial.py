# Generated by Django 4.2.3 on 2023-07-19 07:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("clients", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="MessageDistribution",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "text",
                    models.TextField(
                        verbose_name="Текст сообщения для доставки клиенту"
                    ),
                ),
                ("operator_code", models.CharField(max_length=3)),
                ("tag", models.CharField(max_length=10)),
                ("distribution_start", models.DateTimeField()),
                ("distribution_end", models.DateTimeField()),
            ],
            options={
                "default_related_name": "message_distributions",
            },
        ),
        migrations.CreateModel(
            name="Message",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("created_at", models.DateTimeField(auto_now_add=True)),
                ("is_distributed", models.BooleanField(default=False)),
                (
                    "client",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE, to="clients.client"
                    ),
                ),
                (
                    "distribution",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="notifications.messagedistribution",
                    ),
                ),
            ],
            options={
                "default_related_name": "messages",
            },
        ),
    ]
