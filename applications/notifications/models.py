from django.db import models
from applications.clients.models import Client


class MessageDistribution(models.Model):
    text = models.TextField("Текст сообщения для доставки клиенту")
    operator_code = models.CharField(max_length=3)
    tag = models.CharField(max_length=10)
    distribution_start = models.DateTimeField()
    distribution_end = models.DateTimeField()

    def __str__(self):
        return str(self.id)

    class Meta:
        default_related_name = "message_distributions"


class Message(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    is_distributed = models.BooleanField(default=False)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    distribution = models.ForeignKey(MessageDistribution, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.client} -> {self.distribution}"

    class Meta:
        default_related_name = "messages"
