from django import forms
from .models import MessageDistribution
from applications.clients.models import Client


class DistributionForm(forms.ModelForm):
    class Meta:
        model = MessageDistribution
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["operator_code"].widget = forms.Select(
            choices=self.get_operator_codes()
        )
        self.fields["tag"].widget = forms.Select(choices=self.get_tags())

    def get_operator_codes(self):
        codes = Client.objects.values_list("id", "operator_code").distinct(
            "operator_code"
        )
        return codes

    def get_tags(self):
        tags = Client.objects.values_list("id", "tag").distinct("tag")
        return tags
