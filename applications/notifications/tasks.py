from dataclasses import asdict, dataclass
from typing import List

import requests
from celery import shared_task
from django.conf import settings

from applications.clients.models import Client

from .models import Message, MessageDistribution


@dataclass
class Msg:
    id: int
    phone: int
    text: str

    def as_dict(self):
        return asdict(self)


@shared_task()
def _create_messages(pk):
    distribution = MessageDistribution.objects.get(pk=pk)
    messages = [
        Message(client=client, distribution=distribution)
        for client in Client.objects.filter(operator_code=distribution.operator_code)
    ]

    Message.objects.bulk_create(messages)

    timeout = (distribution.distribution_end - distribution.distribution_start).total_seconds()

    return send_messages.delay(pk, timeout)


@shared_task()
def send_messages(distribution_id, timeout: float) -> list:
    messages = Message.objects.filter(distribution=distribution_id)
    result = []
    timed_out = []
    with requests.Session() as session:
        session.headers.update({"Authorization": settings.SENDER_SERVER_TOKEN})
        for message in messages:
            msg = Msg(
                message.pk, message.client.phone, message.distribution.text
            ).as_dict()
            try:
                response = session.post(
                    settings.SENDER_SERVER_URL + str(message.pk),
                    json=msg,
                    timeout=timeout,
                )
                assert response.status_code < 500, 'Server not working'
                message.is_distributed = True
                message.save()
                result.append(response.text)
            except requests.exceptions.ReadTimeout:
                timed_out.append(msg)
            except AssertionError as e:
                print(e)
    return {"success": result, "timed_out": timed_out}
