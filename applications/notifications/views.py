from rest_framework import viewsets
from .serializers import MessageDistributionSerializer
from .models import MessageDistribution


class MessageDistributionViewSet(viewsets.ModelViewSet):
    queryset = MessageDistribution.objects.all()
    serializer_class = MessageDistributionSerializer
