from rest_framework import routers
from .views import MessageDistributionViewSet


router = routers.DefaultRouter()
router.register("distribution", MessageDistributionViewSet, "distribution")

urlpatterns = router.urls
