FROM python:3.11-slim

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

WORKDIR /code


COPY requirements.txt /code/
COPY . /code/

RUN apt-get -y update && apt-get -y install make

RUN pip install --no-cache-dir -r requirements.txt
RUN python manage.py collectstatic --no-input
