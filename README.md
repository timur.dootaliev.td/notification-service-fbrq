# Тестовое задание от Фабрики Решений

```python
Из дополнительного функционала реализовано запуск через docker-compose(3 пункт)

Описание эндпоинтов через swagger (5 пункт) (доступно по домашней странице)
```

## Run with docker

### Create `.env` file and fill data like in `.env.template`

```bash
cat .env.template > .env
```

```bash
docker-compose up
```

### Create admin user in docker to have access to admin panel

```bash
docker-compose exec web python3 manage.py createsuperuser
```

### Create random clients

```bash
docker-compose exec web make clients
```

## How to run without docker

### Create virtual environment and activate it

```bash
python3 -m venv venv

# linux
source venv/bin/activate

# windows
source venv/Scripts/activate
```

### Install dependencies

```bash
pip install -r requirements.txt
```

### Create database

```bash
createdb <db_name>
```

### Create `.env` file and fill data like in `.env.template`

```bash
cat .env.template > .env
```

### Run migrations

```bash
make migrate

# if you don't have "make"

python3 manage.py migrate
```

### Run server

```bash
make run

# if you don't have "make"

python3 manage.py runserver
```

### Run celery app

```bash
celery -A config worker
```

### Create random clients data

```bash
python3 manage.py createclients
```

### Create admin user to have access to admin panel

```bash
python3 manage.py createsuperuser
```

### Documentation of project available in home page
